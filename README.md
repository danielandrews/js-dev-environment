### Status
[![Build Status](https://travis-ci.org/DanielASAndrews/js-dev-environment.svg?branch=master)](https://travis-ci.org/DanielASAndrews/js-dev-environment) [![Build status](https://ci.appveyor.com/api/projects/status/wm32lqrjrtrc11yx/branch/master?svg=true)](https://ci.appveyor.com/project/DanielASAndrews/js-dev-environment/branch/master) [![Coverage Status](https://coveralls.io/repos/github/DanielASAndrews/js-dev-environment/badge.svg?branch=master)](https://coveralls.io/github/DanielASAndrews/js-dev-environment?branch=master)

# js-dev-environment
Javascript Development Environment
